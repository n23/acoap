#!/usr/bin/env python3
#
# acoap - COAP library for asyncio
# 
# Copyright (C) 2021 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import asyncio
import logging
import typing as tp

import acoap

logger = logging.getLogger()

async def run(uri: str, dtls: tp.Optional[acoap.DTLSContext], observe=False):
    conn, path = acoap.Connection.from_string(uri, dtls=dtls)
    async with conn:
        if observe:
            async for data in conn.observe(path):
                print(data)
        else:
            data = await conn.get(path)
            print(data)

parser = argparse.ArgumentParser()
parser.add_argument(
    '--verbose', action='store_true', help='Verbose debug information output'
)
parser.add_argument('uri')
parser.add_argument('--observe', action='store_true')
parser.add_argument('--psk')
parser.add_argument('--psk-username')

args = parser.parse_args()

level = logging.DEBUG if args.verbose else logging.WARNING
logging.basicConfig(level=level)

ctx = acoap.DTLSContext(args.psk_username, args.psk) if args.psk else None
asyncio.run(run(args.uri, dtls=ctx, observe=args.observe))

# vim: sw=4:et:ai
