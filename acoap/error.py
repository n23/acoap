#
# acoap - COAP library for asyncio
#
# Copyright (C) 2021 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

class COAPError(Exception):
    """
    Basic exception.
    """

class DTLSError(COAPError):
    """
    DTLS error.

    Usually error coming from GnuTLS library.
    """

class MessageFormatError(COAPError):
    """
    Message format error.
    """

# vim: sw=4:et:ai
