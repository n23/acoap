#
# acoap - COAP library for asyncio
#
# Copyright (C) 2021 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Functions for various encoders and decoders.

Supported encoders

- increasing COAP message id
- delta encoding
- COAP options

Supported decoders

- COAP options
"""

import array
cimport cython

from .error import MessageFormatError

ctypedef fused sequence:
    cython.list
    cython.tuple

def message_id(unsigned int id) -> int:
    """
    Increase COAP message id.

    :param id: Input message id.
    """
    return 0xffff & (id + 1)

def delta_encode(list data) -> array.array:
    """
    Delta encode list of integers.

    :param data: List of input integers.
    """
    cdef unsigned short[:] result = array.array('H', data)
    cdef ssize_t size = len(data)
    cdef unsigned char last
    cdef unsigned char current

    if size == 0:
        return result

    last = result[0] = data[0]
    for i in range(1, size):
        current = result[i]
        result[i] = current - last
        last = current

    return result

def encode_options(sequence options) -> bytes:
    """
    Encode collections of COAP options.

    :param options: List or tuple of COAP options.
    """
    items = zip(delta_encode([o.number for o in options]), options)
    return b''.join(encode_option(n, o.value) for n, o in items)

def encode_option(unsigned short num_delta, bytes value) -> bytes:
    """
    Encode COAP option number delta and its value.

    :param num_delta: Delta of COAP option number.
    :param value: COAP option value.
    """
    cdef unsigned short length = len(value)
    cdef unsigned char head[5]
    cdef unsigned char head_len
    cdef unsigned short v

    assert length <= 0xffff

    if num_delta < 13:
        head[0] = num_delta << 4
        head_len = 1
    elif num_delta < 269:
        head[0] = 13 << 4
        head[1] = num_delta - 13
        head_len = 2
    else:
        head[0] = 14 << 4
        v = num_delta - 269
        head[1] = v >> 8
        head[2] = v & 0xff
        head_len = 3

    if length < 13:
        head[0] = head[0] | length
    elif length < 269:
        head[0] = head[0] | 13
        head[head_len] = length - 13
        head_len += 1
    else:
        head[0] = head[0] | 14
        v = length - 269
        head[head_len] = v >> 8
        head[head_len + 1] = v & 0xff
        head_len += 2

    assert head[0] != 0xff and 1 <= head_len <= 5

    return head[0:head_len] + value

def decode_option(bytes data, unsigned int pos) -> tuple[int, bytes, int]:
    """
    Decode binary data of COAP option.

    Return tuple of values

    - COAP option number delta
    - COAP option value
    - position of next COAP option

    :param data: Binary data containing COAP option.
    :param pos: Starting position of COAP option data.
    """
    cdef ssize_t size = len(data)
    cdef unsigned char head = data[pos]
    cdef unsigned char head_pos = pos + 1
    cdef unsigned short num_delta = head >> 4
    cdef unsigned short length = head & 0x0f
    cdef unsigned short data_end
    cdef unsigned short opt_value

    if num_delta == 15:
        raise MessageFormatError('Invalid value of option number delta')
    elif num_delta == 14:
        if not head_pos + 1 < size:
            raise MessageFormatError('Option delta number is missing data')

        opt_value = data[head_pos] << 8 | data[head_pos + 1]
        if opt_value > 0xffff - 269:
            raise MessageFormatError('Overflow of option number delta')
        num_delta = opt_value + 269
        head_pos += 2
    elif num_delta == 13:
        if not head_pos < size:
            raise MessageFormatError('Option delta number is missing data')

        num_delta = data[head_pos] + 13
        head_pos += 1

    if length == 15:
        raise MessageFormatError('Invalid value of option value length')
    elif length == 14:
        if not head_pos + 1 < size:
            raise MessageFormatError('Option value length is missing data')

        opt_value = data[head_pos] << 8 | data[head_pos + 1]
        if opt_value > 0xffff - 269:
            raise MessageFormatError('Overflow of option value length')
        length = opt_value + 269
        head_pos += 2
    elif length == 13:
        if not head_pos < size:
            raise MessageFormatError('Option value length is missing data')

        length = data[head_pos] + 13
        head_pos += 1

    data_end = head_pos + length
    if len(data) < data_end:
        raise MessageFormatError('Invalid length of option value')
    return num_delta, data[head_pos:data_end], data_end

def decode_options(
        bytes data,
        unsigned int pos
    ) -> tuple[list[tuple[int, bytes]], int]:
    """
    Decode multiple COAP options.

    The function returns tuple of

    - list of tuples: (option number, option value)
    - position of first byte after all decoded options

    :param data: Binary data containing COAP options.
    :param pos: Starting position of first COAP option to decode.
    """
    cdef unsigned short number = 0
    cdef list result = []
    while pos < len(data) and data[pos] != 0xff:
        num_delta, value, pos = decode_option(data, pos)
        if 0xffff - num_delta < number:
            raise MessageFormatError('Overflow of option number')
        number += num_delta
        result.append((number, value))

    return result, pos

# vim: sw=4:et:ai
