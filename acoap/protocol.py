#
# acoap - COAP library for asyncio
#
# Copyright (C) 2021 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Implementation of COAP message transmission between COAP endpoints.

..seealso:: https://www.rfc-editor.org/rfc/rfc7252.html#section-4
"""

import asyncio
import dataclasses as dtc
import logging
import typing as tp

from .message import Message, MessageType, decode, encode
from .data import COAPTransport

logger = logging.getLogger(__name__)

class COAPProtocol(asyncio.DatagramProtocol):
    def __init__(self):
        self.transport: tp.Optional[COAPTransport] = None
        self._tasks: tp.Dict[int, RequestTask] = {}
        self._loop = asyncio.get_event_loop()

    def datagram_received(self, data, addr):
        response = decode(data)
        if __debug__:
            logger.debug('received message: {}'.format(response))

        rt = self._tasks.get(response.id)
        if rt is None:
            self._send_reset(response.id)

        request = rt.request
        task = rt.task

        assert request.id == response.id

        # NOTE: implementation detail - keep the conditions structure flat;
        # this is quite verbose, but will be much better with pattern
        # matching when acoap requires python 3.10
        if response.type == MessageType.ACK and response.token == request.token:
            task.set_result(response)
        elif response.type == MessageType.ACK and response.token != request.token:
            logger.warning('received response with invalid token')
        else:
            logger.warning('ignoring response message: {}'.format(response))

    def send(self, msg: Message) -> asyncio.Future:
        assert self.transport is not None
        rt = RequestTask(msg, self._loop.create_future())
        self._tasks[msg.id] = rt
        self.transport.sendto(encode(msg))
        return rt.task

@dtc.dataclass
class RequestTask:
    """
    COAP request message and asynchronous future waiting for response.

    :var request: Message sent to server.
    :var task: Asyncio future waiting for response from server.
    """
    request: Message
    task: asyncio.Future

# vim: sw=4:et:ai
