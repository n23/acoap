#
# acoap - COAP library for asyncio
#
# Copyright (C) 2021 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from __future__ import annotations

import asyncio
import random
import secrets
import typing as tp
from collections.abc import AsyncGenerator, Sequence
from urllib.parse import urlparse

from ._codec import message_id  # type: ignore
from .data import COAPTransport
from .dtls import create_transport, DTLSContext, DTLSTransport
from .message import Message, MessageType, MessageCode, Option, OptionNumber, \
    Options, encode_path, decode_payload
from .protocol import COAPProtocol

class Connection:
    def __init__(
            self,
            host: str,
            port: tp.Optional[int]=None,
            dtls: tp.Optional[DTLSContext]=None
    ) -> None:

        self._host = host
        self._dtls = dtls
        self._protocol = COAPProtocol()

        if self._dtls and port is None:
            self._port = 5684
        elif not self._dtls and port is None:
            self._port = 5683
        elif port is not None:
            self._port = port
        else:
            assert False

        self._message_id = random.randint(0, 0xffff)

    async def get(self, path: str) -> Message:
        options = encode_path(path)
        msg = self._create_msg(options)
        task = self._protocol.send(msg)
        msg = await task
        return decode_payload(msg)

    async def observe(self, path: str) -> AsyncGenerator[Message, None]:
        path_opt = encode_path(path)
        options_start = [Options.OBSERVE_START.value, *path_opt]
        options_end = [Options.OBSERVE_END.value, *path_opt]

        msg_start = self._create_msg(options_start)
        msg_end = self._create_msg_end(msg_start, options_end)

        self._protocol.send(msg_start)
        try:
            while True:
                msg = await self._read()
                yield decode_payload(msg)
        finally:
            self._protocol.send(msg_end)

    async def _read(self) -> Message:
        msg = await self._queue.get()
        if msg.type == MessageType.CON:
            self._protocol.send(self._create_msg_ack(msg))
        return msg

    async def __aenter__(self) -> Connection:
        assert self._port is not None

        transport: COAPTransport

        if self._dtls:
            transport = await create_transport(self._dtls, self._protocol, self._host, self._port)
        else:
            loop = asyncio.get_event_loop()
            _transport, _ = await loop.create_datagram_endpoint(
                lambda: self._protocol, remote_addr=(self._host, self._port)
            )
            transport = tp.cast(COAPTransport, _transport)

        self._protocol.transport = transport
        return self

    async def __aexit__(self, *args):
        return None

    def _create_msg(
            self,
            options: Sequence[Option],
            /,
            type=MessageType.CON,
            code=MessageCode.GET,
        ) -> Message:
        token = secrets.token_bytes(4)  # FIXME: make token length configurable
        return Message(type, code, self._next_message_id(), token=token, options=options)

    def _create_msg_ack(self, msg: Message) -> Message:
        ack = Message(MessageType.ACK, MessageCode.EMPTY, msg.id, token=msg.token)
        return ack

    def _create_msg_end(self, msg: Message, options=list[Option]) -> Message:
        end = Message(
            MessageType.NON,
            MessageCode.GET,
            self._next_message_id(),
            token=msg.token,
            options=options
        )
        return end

    def _next_message_id(self) -> int:
        self._message_id = message_id(self._message_id)
        return self._message_id

    @classmethod
    def from_string(cls, uri_str: str, /, dtls: tp.Optional[DTLSContext]=None) -> tp.Tuple[Connection, str]:
        uri = urlparse(uri_str)
        if uri.scheme == 'coaps':
            assert dtls is not None
        assert uri.hostname
        return Connection(uri.hostname, port=uri.port, dtls=dtls), uri.path

# vim: sw=4:et:ai
