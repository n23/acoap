#
# acoap - COAP library for asyncio
#
# Copyright (C) 2021 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# see https://www.rfc-editor.org/rfc/rfc7252.html#section-3
VERSION = 0x40

# see https://www.rfc-editor.org/rfc/rfc7252.html#section-3
PAYLOAD_MARKER = 0xff

# see https://www.rfc-editor.org/rfc/rfc7252.html#section-4.8
ACK_TIMEOUT = 2
ACK_RANDOM_FACTOR = 1.5
MAX_RETRANSMIT = 4
NSTART = 1
DEFAULT_LEISURE = 5
PROBING_RATE = 1

# see https://www.rfc-editor.org/rfc/rfc7252.html#section-4.8.2
MAX_TRANSMIT_SPAN = ACK_TIMEOUT * ((2 ** (MAX_RETRANSMIT + 1)) - 1) \
    * ACK_RANDOM_FACTOR
MAX_LATENCY = 100
PROCESSING_DELAY = ACK_TIMEOUT
MAX_RTT = (2 * MAX_LATENCY) + PROCESSING_DELAY
EXCHANGE_LIFETIME = MAX_TRANSMIT_SPAN + (2 * MAX_LATENCY) + PROCESSING_DELAY
NON_LIFETIME = MAX_TRANSMIT_SPAN + MAX_LATENCY

# acceptable maximum number of dtls handshake calls
MAX_NUMBER_DTLS_HANDSHAKE = 10

# vim: sw=4:et:ai
