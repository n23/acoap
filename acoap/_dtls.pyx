#
# acoap - COAP library for asyncio
# 
# Copyright (C) 2021 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
GnuTLS functions for DTLS implementation.

Based on https://defuse.ca/gnutls-psk-client-server-example.htm.
"""

import asyncio
import logging
from cpython.bytes cimport PyBytes_FromStringAndSize

from .const import MAX_NUMBER_DTLS_HANDSHAKE
from .error import DTLSError

logger = logging.getLogger(__name__)

DEF MAX_BUFF_LEN = 1024
DEF VARIANT = 'NORMAL:+ECDHE-PSK:+PSK:+ECDHE-ECDSA:+AES-128-CCM-8:+CTYPE-CLI-ALL:+CTYPE-SRV-ALL:+SHA256:-VERS-TLS1.3'

cdef extern from "<gnutls/gnutls.h>":
    enum: GNUTLS_CLIENT
    enum: GNUTLS_CRD_PSK
    enum: GNUTLS_DATAGRAM
    enum: GNUTLS_NONBLOCK
    enum: GNUTLS_PSK_KEY_RAW

    enum: GNUTLS_E_SUCCESS
    enum: GNUTLS_E_AGAIN

    ctypedef struct gnutls_session_t:
        pass

    ctypedef struct gnutls_psk_client_credentials_t:
        pass

    ctypedef struct gnutls_datum_t:
        unsigned char *data
        unsigned int size

    ctypedef enum gnutls_psk_key_flags:
        pass

    ctypedef struct gnutls_transport_ptr_t:
        pass

    ctypedef void (*gnutls_log_func)(int, const char*)
    ctypedef void (*gnutls_audit_log_func)(gnutls_session_t, const char*)

    int gnutls_global_set_log_level(int)
    int gnutls_global_init()
    int gnutls_psk_allocate_client_credentials(gnutls_psk_client_credentials_t*)
    int gnutls_psk_set_client_credentials(gnutls_psk_client_credentials_t, const char*, const gnutls_datum_t*, unsigned int)
    int gnutls_init(gnutls_session_t*, unsigned int)
    int gnutls_set_default_priority_append(gnutls_session_t, const char*, const char **, unsigned int)
    int gnutls_credentials_set(gnutls_session_t, unsigned int, void*)
    void gnutls_transport_set_int(gnutls_session_t, int)
    int gnutls_handshake(gnutls_session_t)
    ssize_t gnutls_record_send(gnutls_session_t, const void*, size_t)
    ssize_t gnutls_record_recv(gnutls_session_t, void*, size_t)

    const char * gnutls_strerror(int)
    void gnutls_global_set_log_level(int)
    void gnutls_global_set_log_function(gnutls_log_func)
    void gnutls_global_set_audit_log_function(gnutls_audit_log_func)

cdef extern from "<gnutls/dtls.h>":
    unsigned int gnutls_dtls_get_timeout(gnutls_session_t)

cdef class Session:
    cdef gnutls_session_t session

cdef void gnutls_audit_log(gnutls_session_t g_session, const char* text):
    logger.debug('gnutls audit: {}'.format(text))

cdef void gnutls_log(int level, const char* text):
    logger.debug('gnutls log:{}: {}'.format(level, text))

def check_error(int errno) -> None:
    """
    Check GnuTLS library result and raise exception on error.

    :param errno: GnuTLS library result.
    """
    cdef bytes msg = gnutls_strerror(errno)
    if errno != GNUTLS_E_SUCCESS and msg is not None:
        raise DTLSError('DTLS error({}): {}'.format(errno, msg.decode()))
    elif errno != GNUTLS_E_SUCCESS and msg is None:
        raise DTLSError('DTLS error({})'.format(errno))

async def dtls_init(bytes username, bytes key, int socket_fd, unsigned char debug) -> Session:
    cdef gnutls_session_t g_session
    cdef gnutls_psk_client_credentials_t psk_cred
    cdef gnutls_datum_t key_data = gnutls_datum_t(key, len(key))
    cdef const char *err
    cdef int result
    cdef unsigned int timeout

    session = Session()

    if debug:
        gnutls_global_set_log_level(9)
        gnutls_global_set_log_function(gnutls_log)
        gnutls_global_set_audit_log_function(gnutls_audit_log)

    result = gnutls_global_init()
    check_error(result)

    result = gnutls_psk_allocate_client_credentials(&psk_cred)
    check_error(result)
    result = gnutls_psk_set_client_credentials(
        psk_cred, username, &key_data, GNUTLS_PSK_KEY_RAW
    )
    check_error(result)

    result = gnutls_init(
        &g_session, GNUTLS_CLIENT | GNUTLS_DATAGRAM | GNUTLS_NONBLOCK
    )
    check_error(result)
    result = gnutls_set_default_priority_append(g_session, VARIANT.encode(), &err, 0)
    check_error(result)
    result = gnutls_credentials_set(g_session, GNUTLS_CRD_PSK, <void*>psk_cred)
    check_error(result)

    gnutls_transport_set_int(g_session, socket_fd)

    # raise error if number of maximum handshake calls is exceeded
    for i in range(MAX_NUMBER_DTLS_HANDSHAKE):
        result = gnutls_handshake(g_session)
        if result == GNUTLS_E_AGAIN:
            timeout = gnutls_dtls_get_timeout(g_session)
            logger.debug(
                'Waiting {} ms for handshake process to finish'.format(timeout)
            )
            await asyncio.sleep(timeout / 1000)
        else:
            break
    check_error(result)

    session.session = g_session
    return session

def dtls_send(Session session, bytes data) -> int:
    cdef int result
    cdef char* buff = data
    result = gnutls_record_send(session.session, buff, len(data))
    return result

def dtls_recv(Session session):
    cdef ssize_t size
    cdef char buff[MAX_BUFF_LEN + 1]
    result = gnutls_record_recv(session.session, buff, MAX_BUFF_LEN)
    return PyBytes_FromStringAndSize(<char*>buff, result)

# vim: sw=4:et:ai
