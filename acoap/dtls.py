#!/usr/bin/env python
#
# acoap - COAP library for asyncio
#
# Copyright (C) 2021 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Asyncio Datagram Transport Layer Security (DTLS) transport for UDP sockets
using GnuTLS.

See also

- https://gnutls.org/manual/html_node/Asynchronous-operation.html
"""

import asyncio
import dataclasses as dtc
import socket

from ._dtls import Session, dtls_init, dtls_send, dtls_recv  # type: ignore

@dtc.dataclass(frozen=True)
class DTLSContext:
    username: str
    psk: str

class DTLSTransport:
    def __init__(
            self,
            ctx: DTLSContext,
            session: Session,
            protocol: asyncio.DatagramProtocol,
            sock: socket.socket
    ) -> None:
        loop = asyncio.get_event_loop()
        self.protocol = protocol
        self.sock = sock
        self.session = session
        loop.add_reader(sock.fileno(), self._read_data)

    def sendto(self, data: bytes) -> None:
        result = dtls_send(self.session, data)

    def _read_data(self):
        data = dtls_recv(self.session)
        self.protocol.datagram_received(data, None)


async def create_transport(ctx: DTLSContext, protocol: asyncio.DatagramProtocol, host: str, port: int) -> DTLSTransport:
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setblocking(False)
    sock.connect((host, port))
    session = await dtls_init(ctx.username.encode(), ctx.psk.encode(), sock.fileno(), True)

    return DTLSTransport(ctx, session, protocol, sock)

# vim: sw=4:et:ai
