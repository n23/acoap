#
# acoap - COAP library for asyncio
#
# Copyright (C) 2021 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from __future__ import annotations

import dataclasses as dtc
import enum
import json
import struct
import typing as tp

from ._codec import encode_options, decode_options  # type: ignore
from .const import PAYLOAD_MARKER, VERSION
from .error import MessageFormatError

FMT_HEADER = struct.Struct('!BBH')

# create coap message code byte value
code = lambda cls, detail=0: cls << 5 | detail

@dtc.dataclass(frozen=True)
class Message:
    """
    COAP message.
    """
    type: MessageType
    code: MessageCode
    id: int
    token: bytes=b''
    options: tp.Sequence[Option]=tuple()
    payload: tp.Any=b''

class MessageType(enum.IntEnum):
    """
    COAP message type.
    """
    # NOTE: two bits only, so the enum is exhaustive
    CON = 0x00
    NON = 0x01 << 4
    ACK = 0x02 << 4
    RST = 0x03 << 4

    def __repr__(self) -> str:
        return '<{}.{}: {}>'.format(self.__class__.__name__, self.name, self.value >> 4)

class MessageCode(enum.IntEnum):
    """
    COAP message code.
    """
    EMPTY = code(0)

    # method codes
    GET = code(0, 1)
    POST = code(0, 2)
    PUT = code(0, 3)
    DELETE = code(0, 4)

    # response codes
    CREATED = code(2, 1)
    DELETED = code(2, 2)
    VALID = code(2, 3)
    CHANGED = code(2, 4)
    CONTENT = code(2, 5)
    BAD_REQUEST = code(4)
    UNAUTHORIZED = code(4, 1)
    BAD_OPTION = code(4, 2)
    FORBIDDEN = code(4, 3)
    NOT_FOUND = code(4, 4)
    METHOD_NOT_ALLOWED = code(4, 5)
    NOT_ACCEPTABLE = code(4, 6)
    PRECONDITION_FAILED = code(4, 12)
    REQUEST_ENTITY_TOO_LARGE = code(4, 13)
    UNSUPPORTED_CONTENT_FORMAT = code(4, 15)
    INTERNAL_SERVER_ERROR = code(5)
    NOT_IMPLEMENTED = code(5, 1)
    BAD_GATEWAY = code(5, 2)
    SERVICE_UNAVAILABLE = code(5, 3)
    GATEWAY_TIMEOUT = code(5, 4)
    PROXYING_NOT_SUPPORTED = code(5, 5)

    @property
    def cls(self) -> int:
        return self >> 5

    @property
    def detail(self) -> int:
        return self & 0x1f

    @classmethod
    def _missing_(cls, value):
        raise MessageFormatError('Invalid message code')

    def __repr__(self) -> str:
        return '<{}.{}: {}.{:02d}>'.format(
            self.__class__.__name__, self.name, self.cls, self.detail
        )

class Option(tp.NamedTuple):
    """
    COAP option.
    """
    number: int
    value: bytes

    @property
    def is_criticial(self) -> bool:
        """
        Is option a critical one.

        .. seealso:: https://www.rfc-editor.org/rfc/rfc7252.html#section-5.4.6
        """
        return self.number % 2 == 1

class OptionNumber(enum.IntEnum):
    """
    COAP option numbers.
    """
    OBSERVE = 6
    URI_PATH = 11
    CONTENT_FORMAT = 12

class Options(enum.Enum):
    """
    Predefined options with known values.
    """
    RESERVERD_0 = Option(0x00, b'')

    OBSERVE_START = Option(OptionNumber.OBSERVE, b'')
    OBSERVE_END = Option(OptionNumber.OBSERVE, b'\x01')

    CONTENT_FORMAT_TEXT_PLAIN =  Option(OptionNumber.CONTENT_FORMAT, b'\x00')
    CONTENT_FORMAT_LINK_FORMAT = Option(OptionNumber.CONTENT_FORMAT, b'\x28')
    CONTENT_FORMAT_XML = Option(OptionNumber.CONTENT_FORMAT, b'\x29')
    CONTENT_FORMAT_OCTET_STREAM = Option(OptionNumber.CONTENT_FORMAT, b'\x30')
    CONTENT_FORMAT_EXI = Option(OptionNumber.CONTENT_FORMAT, b'\x2f')
    CONTENT_FORMAT_JSON = Option(OptionNumber.CONTENT_FORMAT, b'\x32')

def decode(data: bytes) -> Message:
    """
    Decode COAP message.

    :param data: COAP message data.
    """
    if len(data) < 4:
        raise MessageFormatError('Not enough data to parse a message')

    first, code, id = FMT_HEADER.unpack_from(data, 0)

    if not first & 0xc0 == VERSION:
        raise MessageFormatError('Invalid version of COAP message')

    token_len = first & 0x0f
    token = data[4:4 + token_len]
    opt_pos = 4 + token_len

    options, opt_pos = decode_options(data, opt_pos)

    # check for payload marker after all options are decoded as an option
    # value can contain payload marker
    if opt_pos == len(data) - 1 and data[opt_pos] == PAYLOAD_MARKER:
        raise MessageFormatError('Payload marker detected without message payload')
    else:
        assert opt_pos >= len(data) or data[opt_pos] == PAYLOAD_MARKER

    return Message(
        MessageType(first & 0x30),
        MessageCode(code),
        id,
        token,
        options=tuple(Option(n, v) for n, v in options),
        payload=data[opt_pos + 1:]  # skip payload marker
    )

def encode(message: Message) -> bytes:
    """
    Encode COAP message.

    :param message: COAP message.
    """
    header = FMT_HEADER.pack(
        VERSION | message.type | len(message.token),
        message.code,
        message.id
    )
    token = message.token
    options = encode_options(message.options)
    payload = message.payload
    return header + token + options + payload

def encode_path(path: str) -> list[Option]:
    """
    Encode URI path as list of COAP options.

    :param path: URI path to encode.
    """
    return [
        Option(OptionNumber.URI_PATH, p.encode('utf-8'))
        for p in path.split('/') if p
    ]

# https://www.rfc-editor.org/rfc/rfc7252.html#section-12.3
CONTENT_TYPE_DECODER = {
    Options.CONTENT_FORMAT_TEXT_PLAIN.value.value: bytes.decode,
    Options.CONTENT_FORMAT_JSON.value.value: json.loads,
}

def decode_payload(msg: Message) -> tp.Any:
    """
    Decode COAP message payload using content format option.

    :param msg: COAP message.
    """
    items = (
        o.value for o in msg.options
        if o.number == OptionNumber.CONTENT_FORMAT
    )
    content_format = next(items, b'')
    decode = CONTENT_TYPE_DECODER.get(content_format, lambda v: v)
    return decode(msg.payload)  # type: ignore

# vim: sw=4:et:ai
