#
# acoap - COAP library for asyncio
#
# Copyright (C) 2021 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Unit tests for COAP messages.
"""

from acoap import message
from acoap.error import MessageFormatError

import pytest

def test_encode_message():
    """
    Test encoding of COAP message.
    """
    options = [
        message.Options.OBSERVE_START.value,
        *message.encode_path('/abc/xyz'),
    ]
    msg = message.Message(
        message.MessageType.CON,
        message.MessageCode.GET,
        1005,
        options=options
    )
    result = message.encode(msg)
    assert b'@\x01\x03\xed`Sabc\x03xyz' == result

def test_decode_message():
    """
    Test decoding binary data of COAP message.
    """
    result = message.decode(b'@\x01\x03\xed`Sabc\x03xyz')
    assert message.MessageType.CON == result.type
    assert message.MessageCode.GET == result.code
    assert 1005 == result.id

    opt1, opt2, opt3 = result.options
    assert 6 == opt1.number
    assert b'' == opt1.value
    assert 11 == opt2.number
    assert b'abc' == opt2.value
    assert 11 == opt3.number
    assert b'xyz' == opt3.value

def test_decode_message_payload():
    """
    Test decoding binary data of COAP message with payload.
    """
    result = message.decode(b'@\x01\x03\xed`Sabc\x03xyz\xfftest-test-test')
    assert message.MessageType.CON == result.type
    assert message.MessageCode.GET == result.code
    assert 1005 == result.id
    assert b'test-test-test' == result.payload

    opt1, opt2, opt3 = result.options
    assert 6 == opt1.number
    assert b'' == opt1.value
    assert 11 == opt2.number
    assert b'abc' == opt2.value
    assert 11 == opt3.number
    assert b'xyz' == opt3.value

def test_decode_message_empty():
    """
    Test decoding binary data of empty COAP message.
    """
    result = message.decode(b'\x40\x00\x00\x00')

    assert message.MessageType.CON == result.type
    assert message.MessageCode.EMPTY == result.code
    assert 0 == result.id
    assert () == result.options
    assert b'' == result.payload

def test_decode_message_invalid_code():
    """
    Test error when decoding binary data of COAP message with invalid
    message code.
    """
    with pytest.raises(MessageFormatError) as ex_ctx:
        message.decode(b'\x40\x0f\x00\x00')

    assert 'Invalid message code' == str(ex_ctx.value)

def test_decode_message_short():
    """
    Test error when decoding binary data of COAP message with no enough
    data.
    """
    for i in range(4):
        with pytest.raises(MessageFormatError) as ex_ctx:
            message.decode(b'\x00' * i)

        assert 'Not enough data to parse a message' == str(ex_ctx.value)

def test_decode_message_invalid_ver():
    """
    Test error when decoding binary data of COAP message with invalid
    version.
    """
    with pytest.raises(MessageFormatError) as ex_ctx:
        message.decode(b'\x00' * 4)

    assert 'Invalid version of COAP message' == str(ex_ctx.value)

def test_decode_message_payload_error():
    """
    Test error when decoding binary data of COAP message with payload
    marker, but no payload.
    """
    with pytest.raises(MessageFormatError) as ex_ctx:
        message.decode(b'\x40\x00\x00\x00\xff')

    assert 'Payload marker detected without message payload' == str(ex_ctx.value)

def test_decode_message_with_option_and_marker():
    """
    Test decoding binary data of COAP message with an option value
    containing payload marker.
    """
    result = message.decode(b'\x40\x00\x00\x00\x61\xff')
    assert [message.Option(0x06, b'\xff')] == list(result.options)

@pytest.mark.parametrize('path', ['/abc/xyz', 'abc/xyz/', '/abc//xyz/'])
def test_encode_uri_path(path):
    """
    Test encoding URI path as COAP options.
    """
    expected = [message.Option(11, b'abc'), message.Option(11, b'xyz')]

    result = message.encode_path(path)
    assert expected == result

def test_decode_payload_unknown():
    """
    Test decoding COAP message payload for unknown content type.
    """
    msg = message.Message(
        message.MessageType.CON,
        message.MessageCode.GET,
        1005,
        payload=b'test'
    )
    result = message.decode_payload(msg)
    assert b'test' == result

def test_decode_payload_text():
    """
    Test decoding COAP message with text payload.
    """
    msg = message.Message(
        message.MessageType.CON,
        message.MessageCode.GET,
        1005,
        options=[message.Options.CONTENT_FORMAT_TEXT_PLAIN.value],
        payload=b'test'
    )
    result = message.decode_payload(msg)
    assert 'test' == result

def test_decode_payload_json():
    """
    Test decoding COAP message with JSON payload.
    """
    msg = message.Message(
        message.MessageType.CON,
        message.MessageCode.GET,
        1005,
        options=[message.Options.CONTENT_FORMAT_JSON.value],
        payload=b'{"test": 1}'
    )
    result = message.decode_payload(msg)
    assert {'test': 1} == result

# vim: sw=4:et:ai
