#
# acoap - COAP library for asyncio
#
# Copyright (C) 2021 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from itertools import product

from acoap import _codec  # type: ignore
from acoap.message import Option
from acoap.error import MessageFormatError

import pytest

def options_data():
    """
    Binary data for COAP options and their expected values after parsing.

    The expected values are

    - option number delta
    - option value
    - position after option was parsed successfully
    """
    return [
        (b'\x00', (0, b'', 1)),
        (b'\x60', (6, b'', 1)),
        (b'\x610', (6, b'0', 2)),
        (b'\x611', (6, b'1', 2)),

        (b'\x14time', (1, b'time', 5)),
        (b'\x1c012345678901', (1, b'012345678901', 13)),
        (b'\x1d\x000123456789012', (1, b'0123456789012', 15)),
        (b'\x1e\n\xab' + b'0123456789' * 300, (1, b'0123456789' * 300, 3003)),

        (b'\xc4time', (12, b'time', 5)),
        (b'\xd4\x00time', (13, b'time', 6)),
        (b'\xd4\x01time', (14, b'time', 6)),
        (b'\xe4\n\xabtime', (3000, b'time', 7)),

        (b'\xee\n\xb0\n\xab' + b'0123456789' * 300, (3005, b'0123456789' * 300, 3005)),
    ]

def test_delta_encode():
    """
    Test delta encoding of integer numbers.
    """
    data = [1, 2, 3, 4, 5, 5]
    assert [1] * 5 + [0] == list(_codec.delta_encode(data))

@pytest.mark.parametrize(
    'num_delta,value,expected',
    ((num, value, data) for data, (num, value, _) in options_data()),
)
def test_encode_option(num_delta, value, expected):
    """
    Test encoding of single COAP option.
    """
    assert _codec.encode_option(num_delta, value) == expected

def test_encode_options():
    """
    Test encoding of list of COAP options.
    """
    expected = b'`Ttime'
    options = [Option(6, b''), Option(11, b'time')]
    result = _codec.encode_options(options)
    assert expected == result

def test_encode_options_empty():
    """
    Test encoding empty collection of COAP options.
    """
    assert b'' == _codec.encode_options(())
    assert b'' == _codec.encode_options([])

@pytest.mark.parametrize(
    'prefix,data,expected',
    ((p, d, e) for p, (d, e) in product([b'', b'\x00\x00\x00'], options_data()))
)
def test_decode_option(prefix, data, expected):
    """
    Test decoding binary data of a single COAP option.
    """
    len_prefix = len(prefix)
    num_delta, value, pos = expected
    result = _codec.decode_option(prefix + data, len_prefix)
    assert result == (num_delta, value, pos + len_prefix)

@pytest.mark.parametrize(
    'data,error_message',
    [
        (b'\xe0\xff\xff', 'Overflow of option number delta'),
        (b'\x0e\xff\xff', 'Overflow of option value length'),

        (b'\xe0\xff', 'Option delta number is missing data'),
        (b'\xe0', 'Option delta number is missing data'),
        (b'\xd0', 'Option delta number is missing data'),

        (b'\x0e\xff', 'Option value length is missing data'),
        (b'\x0e', 'Option value length is missing data'),
        (b'\x0d', 'Option value length is missing data'),
    ]
)
def test_decode_option_format_error(data, error_message):
    """
    Test decoding single COAP option binary data with format error.
    """
    with pytest.raises(MessageFormatError) as ex_ctx:
        _codec.decode_options(data, 0)

    assert str(ex_ctx.value) == error_message

#
# Unit tests for decoding multiple options
#
def test_decode_options():
    """
    Test decoding COAP options binary data.
    """
    expected = [(6, b''), (11, b'time')]
    result, pos = _codec.decode_options(b'`Ttime', 0)
    assert expected == result
    assert 6 == pos

def test_decode_options_num_delta_edge():
    """
    Test decoding COAP options binary data with valid option number at edge
    of its valid value.
    """
    # at edge of 0xffff: (0xfde5 + 269) + (0 + 269) == 0xffff
    result = _codec.decode_options(b'\xe0\xfd\xe5\xe0\x00\x00', 0)
    assert [(0xfde5 + 269, b''), (0xffff, b'')] == result[0]

@pytest.mark.parametrize(
    'data,error_message',
    [
        (b'\xe0\xfe\xf2\xe0\xfe\xf2', 'Overflow of option number'),
        # (0xfde5 + 269) + (0x0001 + 269) > 0xffff
        (b'\xe0\xfd\xe5\xe0\x00\x01', 'Overflow of option number'),
        (b'\xf0', 'Invalid value of option number delta'),
        (b'\x0f', 'Invalid value of option value length'),
        (b'\x61', 'Invalid length of option value'),
    ],
)
def test_decode_options_format_errors(data, error_message):
    """
    Test decoding COAP options binary data with format errors.
    """
    with pytest.raises(MessageFormatError) as ex_ctx:
        _codec.decode_options(data, 0)

    assert str(ex_ctx.value) == error_message

# vim: sw=4:et:ai
